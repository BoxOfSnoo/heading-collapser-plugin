#Heading Collapser Plugin for Wordpress

This is a *very* simple plugin to allow me to dynamically reformat posts by making H3 headings clickable toggles for the text underneath them.
You need Twitter Bootstrap included in your theme before this will work.

Currently this is hardcoded to only use H3 tags, but there is the skeleton of an admin/settings interface already there.

Also hardcoded is the option to automatically expand the first element of the accordion. It is forced off.
