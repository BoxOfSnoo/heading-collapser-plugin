<?php 

/*
 * Not currently implemented.  Heading type needs some refactoring of the plugin to work at all
 * and the checkbox for expand_first needs debugging.
 */

function heading_collapser_options_set() {
	set_option('heading_to_collapse','Heading Type');
	set_option('expand_first','Automatically Expand First Heading');
}

function heading_collapser_options_unset() {
	delete_option('heading_to_collapse');
	delete_option('expand_first');
}

register_activation_hook(__FILE__,'heading_collapser_options_set');
register_deactivation_hook(__FILE__,'heading_collapser_options_unset');

function heading_collapser_options() {

	echo '<div class="wrap"><h2>Heading Collapser Options</h2>';
	if ($_REQUEST['submit']) {
		heading_collapser_options_update();
	}
	heading_collapser_show_form();
	echo '</div>';
}

function heading_collapser_show_form() {
	$default_heading = get_option('heading_to_collapse');
	if (!$default_heading) {
		$default_heading = '3';
	}
	$default_expand_first = get_option('expand_first');
	
	echo '<form method="post">
		<br />
		<label for="heading_to_collapse">Heading to collapse:
		<select name="heading_to_collapse">';
	for ($i=2; $i<=4; $i++) {
		printf('<option value="%1s" %2s>Heading %3s</option>',$i, ($default_heading==$i)?'selected="selected"':'', $i);
	}
	echo '</select>
		<br />
		<label for="expand_first">Automatically Expand First Heading? ';
	printf('<input type="checkbox" name="expand_first"%1s>', (empty($default_expand_first))?'checked':'');
	echo '<p class="submit">
		<input type="submit" name="submit" value="Save Options" />
		</p>
		</form>';
		
}


function heading_collapser_options_update() {
	if ($_REQUEST['heading_to_collapse']) {
		update_option('heading_to_collapse',$_REQUEST['heading_to_collapse']);
		$ok = true;
	}
	
	if ($_REQUEST['expand_first']) {
		update_option('expand_first',$_REQUEST['expand_first']);
		$ok = true;
	}
	
	if ($ok) {
		echo '<div id="message" class="updated fade">
				<p>Options saved.</p>
				</div>';
	}
}


function heading_collapser_modify_menu() {
	add_options_page(
		'Headings Collapser', // page title
		'Headings Collapser', // submenu title
		'manage_options',     // access/capability
		__FILE__,				 // file
		'heading_collapser_options' // function to hook
		);
}

add_action('admin_menu','heading_collapser_modify_menu');

// Add settings link on plugin page
function your_plugin_settings_link($links) { 
  $settings_link = '<a href="options-general.php?page=h3collapser.php">Settings</a>'; 
  array_unshift($links, $settings_link); 
  return $links; 
}
 
$plugin = plugin_basename(__FILE__); 
add_filter("plugin_action_links_$plugin", 'your_plugin_settings_link' );
