<?php
/*
Plugin Name: Heading Collapser
Plugin URI: http://jonandtina.net
Description: Automatically collapse certain headings as an accordion
Version: 1.0.2
Author: Jonathan Markevich
Author URI: http://jonandtina.net
License: GPL2
*/


/*
 * The following require will include a settings page.  This works fine but the code below
 * would need to be changed to end the child (collapsed) container at any heading tag and not 
 * just the same one as the opening tag.
 */
//require_once('admin/settings.php');

function heading_collapser_filter($content) {

	if (!is_feed() && !is_home()) {
	
		if (is_single()) {

	//		$collapsible_heading = get_option('heading_to_collapse');
	
			// Add a class to the H3 tags
			$content = preg_replace('/<h3(.*)>(.*)\/h3>/i', '<h3${1} class="heading-collapse">${2}/h3>', $content);
		
			// Split content into an array delimited by tags
			$split_content = preg_split('/(<[^>]*[^\/]>)/i', $content, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
		
			// Initialize flags
			$open_div = false;
			$open_h3 = false;
//			$parent_wrapper = false;
//
//			$expand_first = true;
			$first_tag = false; // loop flag but can also force off first-heading expansion

			// Initialize strings
			$new_content = "";
			$buffer = "";

			for ($i = 0; $i < count($split_content); $i++) {
			
				/* 
				 * Look for either opening or closing H3 tags
				 */

	//			$matchstring = sprintf("/<\/*h%1s/i", $collapsible_heading);  // if using configurable settings
				if (preg_match('/<\/*h3/i', $split_content[$i]) ) {
		
					/* 
					 * If the tag is closing, then we need to start a div that we can toggle
					 */
					if (preg_match('/<\/h3>/i',$split_content[$i])) { // Closing H3 tag
						$open_h3 = false;
						
						// Start the collapsing div right after the tag
						if ($first_tag) {
							$buffer = '<div class="accordion-body collapse in" id="collapse'. $h3_target .'">';
							$first_tag = false;
						} else {
							$buffer = '<div class="accordion-body collapse" id="collapse'. $h3_target .'">';
						}
						$open_div = true;
		
					/* 
					 * If the tag is opening, then we probably have some content in the buffer.
					 * dump it and close the div before we print it.
					 */
					} else {
						$open_h3 = true;
						$h3_target = $i;
						$new_content .= $buffer;
	
						/* 
						 * We may have an opening tag but no open div, in the case of blurb text before any H3
						 * in that case, don't close a div.
						 */
						if ($open_div) {
							$new_content .= "</div>";
							$new_content .= "</div>";
							$open_div = false;
						}
	
					}
	
					/*
					 * Write the H3 tag
					 * depending on opening or closing we may need to open or close the A tag
					 */
					if ($open_h3) {
						$new_content .= '<div class="accordion-group">';
						$new_content .= $split_content[$i];
						$new_content .= '<a href="#collapse' . $h3_target .'" data-toggle="collapse" class="accordion-toggle collapsed">';
					} else {
						$new_content .= "</a>";
						$new_content .= $split_content[$i];
					}
		
				/*
				 * We have any other content other than H3 tags
				 */
				} else {
					/*
					 * if the H3 isn't closed, we are looking at the contents of the tag
					 */
					if ($open_h3) {
						$new_content .= $split_content[$i]; // Write the contents of the H3 tag
					}
					$buffer .= $split_content[$i];
				}
			}
		
			/*
			 * Write anything after the last H3
			 * If we didn't have any H3s in the whole document, then we still need to output the buffer
			 */
			$new_content .= $buffer;
		
			/* 
			 * Close any trailing div
			 */	
			if ($open_div) {
				$new_content .= "</div>"; // Close section div
			}
	
			$content = $new_content;
		}
	}	
	return $content;
}


/*
 * Notify wordpress of our plugin and style
 */
wp_register_style('heading-collapser', plugins_url('heading-collapser.css',__FILE__ ));
wp_enqueue_style('heading-collapser');

wp_enqueue_script('heading-collapser', plugins_url('heading-collapser.js',__FILE__),array('jquery'),false,true); // load in footer

add_filter('the_content', 'heading_collapser_filter');
